# cronlist [![pipeline status](https://gitlab.com/victor-engmark/cronlist/badges/master/pipeline.svg)](https://gitlab.com/victor-engmark/cronlist/-/commits/master) [![coverage report](https://gitlab.com/victor-engmark/cronlist/badges/master/coverage.svg)](https://gitlab.com/victor-engmark/cronlist/-/commits/master)


`cronlist` lists upcoming cron actions from `/etc/crontab` and your personal `crontab`. By default it prints the next ten actions. Run `cronlist --help` for help.

## Warning

This implementation is not timezone aware, so don't expect it to work in the presence of:

- `CRON_TZ`
- Daylight savings time

## Installation

    git submodule update --init
    make
    sudo make install

## Rust alpha notes

Build, test, lint & format:

    make --file=rust.mk test lint

Build optimized binary:

    make --file=rust.mk release

Run:

    crontab -l | ./target/release/cronlist
